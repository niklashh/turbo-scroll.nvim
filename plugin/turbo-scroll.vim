let prefixes = {
    \ 1: "",
    \ 2: "2-",
    \ 5: "3-",
    \ 7: "4-"
  \ }

let preprefixes = ["", "s-", "c-"]

for [index, prefix] in items(prefixes)
    for dir in ["Up", "Down"]
        for preprefix in preprefixes
            if preprefix == ""
                execute "noremap \<" . prefix . "ScrollWheel" . dir . "> "
                    \ . index . "<" . dir . ">"
            else
                execute "noremap \<" . preprefix . prefix . "ScrollWheel" . dir . "> "
                    \ . index . "<" . preprefix . dir . ">"
            endif
        endfor
    endfor
endfor
