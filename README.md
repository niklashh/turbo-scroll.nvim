# Turbo-Scroll.nvim

Add a turbo to scroll in mousemode `mouse=a`!

## Install

For dein.vim

```vim
call dein#add('https://gitlab.com/niklashh/turbo-scroll.nvim')
```

## Inspiration

A solution to https://github.com/neovim/neovim/issues/6211

## TODO

- [ ] Add configuration variables for
  - [ ] Customizable keys
  - [ ] Customizable number of repetitions
- [ ] More speeed
